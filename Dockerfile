FROM golang:1.17.11-alpine3.16 AS builder

ENV CGO_ENABLED=0

WORKDIR /src
COPY . /src

RUN apk add --no-cache git
RUN go build \
      -ldflags "-X main.REVISION=$(git rev-parse --short=8 HEAD || echo "unknown")" \
      .

FROM alpine:3.16

RUN apk add --no-cache bash curl

COPY --from=builder /src/gcp-cloud-logging-test /usr/bin/
COPY ./entrypoint /entrypoint

ENTRYPOINT ["/entrypoint"]

