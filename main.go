package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	Name = "GCP Cloud Logging test"
)

var (
	REVISION = "HEAD"
)

func main() {
	ctx, cancelCtx := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancelCtx()

	logger := getLogger()
	logger.(logrus.FieldLogger).Printf("Starting %s (revision %s)", Name, REVISION)

	for {
		select {
		case <-ctx.Done():
			logger.WithError(ctx.Err()).Print("Context canceled; exitting")
			return
		case <-time.After(10 * time.Second):
			logger.Printf("Log entry for %s!", time.Now().Format(time.RFC1123Z))
		}
	}
}

func getLogger() logrus.FieldLogger {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: time.RFC3339Nano,
	})

	wd, err := os.Getwd()
	if err != nil {
		wd = fmt.Sprintf("requesting error: %w", err)
	}

	return logger.WithFields(logrus.Fields{
		"pid":      os.Getpid(),
		"ppid":     os.Getppid(),
		"uid":      os.Getuid(),
		"gid":      os.Getgid(),
		"pagesize": os.Getpagesize(),
		"pwd":      wd,
	})
}
