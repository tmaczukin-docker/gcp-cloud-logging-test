# GCP Cloud Logging test

iA small, dummy containerized service for testing integration with GCP Cloud
Logging (especially for containers started on Google COS Compute instances and not GKE).

## Authors

Tomasz Maczukin, 2022

## License

MIT

